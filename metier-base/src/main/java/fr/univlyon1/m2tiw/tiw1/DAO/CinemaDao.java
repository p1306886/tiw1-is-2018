package fr.univlyon1.m2tiw.tiw1.DAO;

import fr.univlyon1.m2tiw.tiw1.metier.Cinema;

/**
 * Interface DAO pour les Cinemas.
 * 
 * @author donatien
 *
 */
public interface CinemaDao {
    public Cinema findByName(String name);
}
