package fr.univlyon1.m2tiw.tiw1.DAO.impl;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univlyon1.m2tiw.tiw1.DAO.CinemaDao;
import fr.univlyon1.m2tiw.tiw1.metier.Cinema;
import fr.univlyon1.m2tiw.tiw1.metier.Film;
import fr.univlyon1.m2tiw.tiw1.metier.Salle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation du DAO Cinema.
 * 
 * @author donatien
 *
 */
public class CinemaDaoImpl implements CinemaDao {

    private File fileSource;
    final Logger logger = LoggerFactory.getLogger(CinemaDaoImpl.class);

    public CinemaDaoImpl(final String filename) {
        fileSource = new File(filename);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Cinema findByName(final String name) {
        ObjectMapper objectMapper = new ObjectMapper();
        LinkedHashMap<String, Object> jsonMap = null;
        LinkedHashMap<String, Object> cineMap = null;
        HashMap<String, Salle> mappingSalle = new HashMap<>();
        HashMap<String, Film> mappingFilm = new HashMap<>();
        Cinema cinema = null;
        try {
            jsonMap = objectMapper.readValue(fileSource, new TypeReference<Map<String, Object>>() {
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        cineMap = (LinkedHashMap<String, Object>) jsonMap.get("cinema");
        cinema = new Cinema((String) cineMap.get("nom"));
        for (LinkedHashMap<String, Object> mapSalle : ((ArrayList<LinkedHashMap<String,
                Object>>) cineMap.get("salles"))) {
            Salle salle = new Salle((String) mapSalle.get("nom"), (int) mapSalle.get("capacite"));
            cinema.addSalle(salle);
            mappingSalle.put(salle.getNom(), salle);
        }

        for (LinkedHashMap<String, Object> mapFilm : ((ArrayList<LinkedHashMap<String,
                Object>>) cineMap.get("films"))) {
            Film film = new Film((String) mapFilm.get("titre"), (String) mapFilm.get("version"),
                    (String) mapFilm.get("fiche"));
            cinema.addFilm(film);
            mappingFilm.put(film.getTitre() + " - " + film.getVersion(), film);
        }

        for (LinkedHashMap<String, Object> mapSceance : ((ArrayList<LinkedHashMap<String,
                Object>>) cineMap.get("seances"))) {
            Salle salle = mappingSalle.get((String) mapSceance.get("salle"));
            Film film = mappingFilm.get((String) mapSceance.get("film"));
            DateFormat df = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss +hhss");
            Date date = null;
            try {
                date = df.parse((String) mapSceance.get("date"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Float prix = ((Double) mapSceance.get("prix")).floatValue();
            cinema.createSeance(salle, film, date, prix);
        }
        logger.debug(cinema.toString());
        return cinema;
    }

}
