package fr.univlyon1.m2tiw.tiw1.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Cinéma.
 * 
 * @author nicof
 */
public class Cinema {
    /**
     * Nom du cinéma.
     */
    private final String nom;
    /**
     * Liste des salles.
     */
    private final Map<String, Salle> salles;
    /**
     * Liste des Films.
     */
    private final Map<String, Film> films;
    /**
     * Liste des scéances.
     */
    private final List<Seance> seances;

    /**
     * Class utilisé pour les instances de cinéma.
     * 
     * @param inNom
     *            nom du cinéma
     */
    public Cinema(final String inNom) {
        this.nom = inNom;
        this.salles = new HashMap<String, Salle>();
        this.films = new HashMap<String, Film>();
        this.seances = new ArrayList<Seance>();
    }

    /**
     * Class utilsée par Jackson pour deserialiser.
     * @param nom
     *          nom du cinema
     * @param salles
     *          salles
     * @param films
     *          films
     * @param seances
     *          seances
     */
    public Cinema(String nom, Map<String, Salle> salles,
            Map<String, Film> films, List<Seance> seances) {
        super();
        this.nom = nom;
        this.salles = salles;
        this.films = films;
        this.seances = seances;
    }

    /**
     * Accesseur du nom du cinéma.
     * 
     * @return Nom du cinéma
     */
    public final String getNom() {
        return nom;
    }

    /**
     * Ajoute une Salle au cinéma.
     * 
     * @param salle
     *            salle a ajouter
     */
    public final void addSalle(final Salle salle) {
        this.salles.put(salle.getNom(), salle);
    }

    /**
     * Supprime une Salle au cinéma.
     * 
     * @param salle
     *            salle a Supprimer
     */
    public final void removeSalle(final Salle salle) {
        this.salles.remove((Object) salle);
    }

    /**
     * Ajoute un film au cinéma.
     * 
     * @param film
     *            film a ajouter
     */
    public final void addFilm(final Film film) {
        this.films.put(film.getTitre(), film);
    }

    /**
     * Supprime un film au cinéma.
     * 
     * @param film
     *            film a Supprimer
     */
    public final void removeFilm(final Film film) {
        this.films.remove((Object) film);
    }

    /**
     * Ajoute une nouvelle scéance au cinéma.
     * 
     * @param salle
     *            salle de la scéance
     * @param film
     *            film de la scéance
     * @param date
     *            date de la scéance
     * @param prix
     *            prix de la scéance
     */
    public final void createSeance(final Salle salle, final Film film,
            final Date date, final float prix) {
        this.seances.add(new Seance(film, salle, date, prix));
    }

    @Override
    public String toString() {
        return "Cinema [nom=" + nom + ", salles=" + salles + ", films=" + films + ", seances="
                + seances + "]";
    }

    /**
     * Supprime une scéance du cinéma.
     * 
     * @param seance
     *            scéance a supprimer
     */
    public final void removeSeance(final Seance seance) {
        seances.remove(seance);
    }

    /**
     * renvois le nombre de scéance programé dans le cinéma.
     * 
     * @return int le nombre de scéance
     */
    public final int getNbSeances() {
        return seances.size();
    }
}
