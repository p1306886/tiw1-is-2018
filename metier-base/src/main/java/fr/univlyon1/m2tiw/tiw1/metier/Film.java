package fr.univlyon1.m2tiw.tiw1.metier;

/**
 * classe des films.
 * @author nicof
 *
 */
public class Film {

    @Override
    public String toString() {
        return "Film [titre=" + titre + ", version=" + version + ", fiche=" + fiche + "]";
    }

    /**
     * Titre du film.
     */
    private final String titre;
    /**
     * Langue du film (VO, VF...).
     */
    private final String version;
    /**
     * La fiche du film sur Linked Movie Database.
     */
    private final String fiche;

    /**
     * class Film.
     * @param inTitre titre du film
     * @param inVersion Langue du film (VO, VF...)
     * @param inFiche La fiche du film sur Linked Movie Database
     */
    public Film(final String inTitre, final String inVersion,
            final String inFiche) {
        this.titre = inTitre;
        this.version = inVersion;
        this.fiche = inFiche;
    }

    /**
     * Accesseur du titre du film.
     * @return String : le titre du film
     */
    public final String getTitre() {
        return titre;
    }

    /**
     * Accesseur de la langue film.
     * @return String : Langue du film
     */
    public final String getVersion() {
        return version;
    }

    /**
     * Accesseur de l'url de la fiche du film.
     * @return  String : URL de La fiche du film sur Linked Movie Database
     */
    public final String getFiche() {
        return fiche;
    }
}
