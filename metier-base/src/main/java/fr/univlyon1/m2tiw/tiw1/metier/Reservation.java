package fr.univlyon1.m2tiw.tiw1.metier;

/**
 * classe qui gere les reservasions.
 * @author nicof
 *
 */
public class Reservation {
    @Override
    public String toString() {
        return "Reservation [prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", paye="
                + paye + "]";
    }

    private final String prenom;
    private final String nom;
    private final String email;
    private boolean paye;

    /**
     * Class de gestion des réservation.
     * @param prenom : prenom de la persone qui a fait la reservation
     * @param nom : nom de la persone qui a fait la reservation
     * @param email : email de la persone qui a fait la reservation
     */
    public Reservation(final String prenom, final String nom, final String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.paye = false;
    }

    /**
     * accesseur.
     * @return String : prenom de la persone qui a fait la reservation
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Accesseur.
     * @return String nom de la persone qui a fait la reservation
     */
    public String getNom() {
        return nom;
    }

    /**
     * Accesseur.
     * @return String : email de la persone qui a fait la reservation
     */
    public String getEmail() {
        return email;
    }

    /**
     * Accesseur.
     * @return bolean : la reservation est payé
     */
    public boolean isPaye() {
        return paye;
    }

    /**
     * Setteur.
     * @param paye bolean : défini si la réservation est payé ou non.
     */
    public void setPaye(boolean paye) {
        this.paye = paye;
    }
}
