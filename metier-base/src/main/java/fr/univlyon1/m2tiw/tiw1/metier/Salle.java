package fr.univlyon1.m2tiw.tiw1.metier;

public class Salle {
    @Override
    public String toString() {
        return "Salle [nom=" + nom + ", capacite=" + capacite + "]";
    }

    private final String nom;
    private final int capacite;

    public Salle(String nom, int capacite) {
        this.nom = nom;
        this.capacite = capacite;
    }

    public String getNom() {
        return nom;
    }

    public int getCapacite() {
        return capacite;
    }
}
