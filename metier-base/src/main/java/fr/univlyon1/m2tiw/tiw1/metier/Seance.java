package fr.univlyon1.m2tiw.tiw1.metier;


import fr.univlyon1.m2tiw.tiw1.utils.SeanceCompleteException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seance {
    @Override
    public String toString() {
        return "Seance [film=" + film + ", salle=" + salle + ", date=" + date + ", prix=" + prix
                + ", reservations=" + reservations + "]";
    }

    private final Film film;
    private final Salle salle;
    private final Date date;
    private final float prix;
    private List<Reservation> reservations;

    
    /**
     * Classe Scéance.
     * 
     * @param film : film de la sceance
     * @param salle : salle de la sceance
     * @param date : date de la sceance
     * @param prix : prix de la sceance
     */
    public Seance(Film film, Salle salle, Date date, float prix) {
        this.film = film;
        this.salle = salle;
        this.date = date;
        this.prix = prix;
        this.reservations = new ArrayList<Reservation>();
    }

    public Film getFilm() {
        return film;
    }

    public Salle getSalle() {
        return salle;
    }

    public Date getDate() {
        return date;
    }

    public float getPrix() {
        return prix;
    }

    /**
     * creer une réservation.
     * 
     * @param prenom : prenom pour la reservation
     * @param nom : nom pour la reservation
     * @param email : email pour la reservation
     * @throws SeanceCompleteException : custom error
     */
    public void createReservation(final String prenom, final String nom, final String email)
            throws SeanceCompleteException {
        if (this.salle.getCapacite() >= this.reservations.size()) {
            throw new SeanceCompleteException();
        }
        Reservation resa = new Reservation(prenom, nom, email);
        this.reservations.add(resa);
        resa.setPaye(true);

    }

    public void cancelReservation(Reservation reservation) {
        this.reservations.remove(reservation);
    }
}
