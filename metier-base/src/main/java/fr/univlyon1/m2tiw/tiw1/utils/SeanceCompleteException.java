package fr.univlyon1.m2tiw.tiw1.utils;

/**
 * 
 * Util.
 * 
 * @author donatien
 *
 */
public class SeanceCompleteException extends Exception {
    
    private static final long serialVersionUID = 6049449155765000123L;

    public SeanceCompleteException() {
        super("Cette seance est complete.");
    }
}
