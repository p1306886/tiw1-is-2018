package fr.univlyon1.m2tiw.tiw1.metier;

import org.junit.Test;

import fr.univlyon1.m2tiw.tiw1.DAO.CinemaDao;
import fr.univlyon1.m2tiw.tiw1.DAO.impl.CinemaDaoImpl;

import static org.junit.Assert.assertEquals;

public class CinemaTest {

    private final CinemaDao cinemaDao = new CinemaDaoImpl(
            "src/main/resources/sample-data/mon-cinema.json");
    private final Cinema cinema = cinemaDao.findByName("Mon Cinema");

    @Test
    /**
     * Teste si on a bien 4 séances / jour x 7 jours x 3 salles = 84 séances
     */
    public void getNbSeances() throws Exception {
        assertEquals(84, cinema.getNbSeances());
    }
}
